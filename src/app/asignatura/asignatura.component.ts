import { Component, OnInit } from '@angular/core';
import { AsignaturaService } from './asignatura.service';
import { Asignatura } from './asignatura';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-asignatura',
  templateUrl: './asignatura.component.html'
})
export class AsignaturaComponent implements OnInit {

  idProfesor: any;
  asignaturas: Asignatura[];

  constructor(public asignaturaService: AsignaturaService,
    private activatedRoute: ActivatedRoute) {
    this.asignaturas = [];

  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(param => {
      this.idProfesor = param['idProfesor']
      this.asignaturaService.getAsignatura(this.idProfesor).subscribe(
        asignaturas => this.asignaturas = asignaturas
      );
    });
  }

}
