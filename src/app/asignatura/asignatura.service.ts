import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { Asignatura } from './asignatura';

@Injectable({
  providedIn: 'root'
})
export class AsignaturaService {

  private urlAsignatura = 'http://localhost:9898/api/asignatura';
  constructor(private http: HttpClient) { }

  getAsignatura(id: number): Observable<Asignatura[]>{
    return this.http.get<Asignatura[]>(`${this.urlAsignatura}/${id}`);
  }
}
