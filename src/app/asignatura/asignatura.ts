export class Asignatura {
  idAsignatura: number;
  nombre: string;
  profesor: Profesor;
  curso: Curso;

  constructor(){
    this.idAsignatura = 0;
    this.nombre = '';
    this.curso = new Curso;
    this.profesor = new Profesor;
  }
}

 export class Curso {
    idCurso: number;
    grado:   number;
    salon:   Salon;

    constructor(){
      this.idCurso = 0;
      this.grado = 0;
      this.salon = new Salon;
    }
}

export class Salon {
  salon: string = '';
}

export class Profesor {
    idProfesor: number;
    nombre:     string;

    constructor(){
      this.idProfesor =0;
      this.nombre = '';
    }
}
