import { Component, OnInit } from '@angular/core';
import { AsignaturaEstudianteService } from './asignatura-estudiante.service';
import { AsignaturaEstudiante } from './asignatura-estudiante';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-asignatura-estudiante',
  templateUrl: './asignatura-estudiante.component.html'
})
export class AsignaturaEstudianteComponent implements OnInit {

  idAsignatura: any;
  asignaturasEstudiantes: AsignaturaEstudiante[];

  constructor(public asignaturaEstudianteService: AsignaturaEstudianteService,
    private activatedRoute: ActivatedRoute) {
    this.asignaturasEstudiantes = [];
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(param => {
      this.idAsignatura = param['idAsignatura']
      this.asignaturaEstudianteService.getAsignaturaEstudiante(this.idAsignatura).subscribe(
        asignaturasEstudiantes => this.asignaturasEstudiantes = asignaturasEstudiantes
      );
    })
  }
}
