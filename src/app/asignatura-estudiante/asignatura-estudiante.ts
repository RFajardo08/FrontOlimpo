export class AsignaturaEstudiante {
  idAsignaturaEstudiante: number;
  estudiante: Estudiante;
  asignatura: Asignatura;

  constructor(){
    this.idAsignaturaEstudiante = 0;
    this.estudiante = new Estudiante;
    this.asignatura = new Asignatura;
  }
}

export class Estudiante{
  idEstudiante: number;
  nombre: string;

  constructor(){
    this.idEstudiante = 0;
    this.nombre = '';
  }
}

export class Asignatura{
  idAsignatura: number;
  nombre: string;

  constructor(){
    this.idAsignatura = 0;
    this.nombre = '';
  }
}
