import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { AsignaturaEstudiante } from './asignatura-estudiante';

@Injectable({
  providedIn: 'root'
})
export class AsignaturaEstudianteService {

  private urlAsignatura = 'http://localhost:9898/api/asiest';
  constructor(private http: HttpClient) { }

  getAsignaturaEstudiante(id: number): Observable<AsignaturaEstudiante[]>{
    return this.http.get<AsignaturaEstudiante[]>(`${this.urlAsignatura}/${id}`);
  }
}
