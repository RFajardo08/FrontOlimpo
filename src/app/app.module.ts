import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ColegioComponent } from './colegio/colegio.component';
import { ColegioService } from './colegio/colegio.service';
import { RouterModule, Routes} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ProfesorComponent } from './profesor/profesor.component';
import { AsignaturaComponent } from './asignatura/asignatura.component';
import { CursoComponent } from './curso/curso.component';
import { EstudianteComponent } from './estudiante/estudiante.component';
import { AsignaturaEstudianteComponent } from './asignatura-estudiante/asignatura-estudiante.component';

const routes: Routes = [
  {path: '', redirectTo: '/profesores', pathMatch: 'full'},
  {path: 'colegios', component: ColegioComponent},
  {path: 'profesores', component: ProfesorComponent},
  {path: 'asignaturas', component: AsignaturaComponent},
  {path: 'asignaturas/:idProfesor', component: AsignaturaComponent},
  {path: 'asignaturasEstudiantes/:idAsignatura', component: AsignaturaEstudianteComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ColegioComponent,
    ProfesorComponent,
    AsignaturaComponent,
    CursoComponent,
    EstudianteComponent,
    AsignaturaEstudianteComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ColegioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
