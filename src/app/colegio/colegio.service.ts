import { Injectable } from '@angular/core';
import { COLEGIOS } from './colegios.json';
import { Colegio } from './colegio';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ColegioService {

  private urlColegio: string = 'http://localhost:9898/api/colegio'
  constructor(private http: HttpClient) { }

  getColegios(): Observable<Colegio[]>{
    return this.http.get<Colegio[]>(this.urlColegio);
  }

  getColegioId(id: number): Observable<Colegio>{
    return this.http.get<Colegio>(`${this.urlColegio}/${id}`);
  }
}
