import { Component, OnInit } from '@angular/core';
import { Colegio } from './colegio';
import { ColegioService } from './colegio.service';

@Component({
  selector: 'app-colegio',
  templateUrl: './colegio.component.html'
})
export class ColegioComponent implements OnInit {

  colegios: Colegio[];
  constructor(private colegioService: ColegioService) {
    this.colegios = []  }

  ngOnInit() {
    this.colegioService.getColegios().subscribe(
      colegios => this.colegios = colegios
    );
  }

}
