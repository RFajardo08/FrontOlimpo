import { Colegio } from './colegio';

export const COLEGIOS: Colegio[] = [
  {idColegio: 1, nombre: 'Colegio del Olimpo'},
  {idColegio: 2, nombre: 'Colegio del Tartaro'}
];
