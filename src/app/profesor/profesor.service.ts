import { Injectable } from '@angular/core';
import { Profesor } from './profesor';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfesorService {

  private urlProfesor: string = 'http://localhost:9898/api/profesor';
  constructor(private http: HttpClient) { }

  getProfesor(): Observable<Profesor[]>{
    return this.http.get<Profesor[]>(this.urlProfesor);
  }

  getProfesorId(id: number): Observable<Profesor>{
    return this.http.get<Profesor>(`${this.urlProfesor}/${id}`);
  }
}
