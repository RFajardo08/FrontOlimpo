import { Component, OnInit } from '@angular/core';
import { Profesor } from './profesor';
import { ProfesorService } from './profesor.service';

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html'
})
export class ProfesorComponent implements OnInit {

  profesores: Profesor[];
  constructor(private profesorService: ProfesorService) {
    this.profesores = []
  }

  ngOnInit() {
    this.profesorService.getProfesor().subscribe(
      profesores => this.profesores = profesores
    );
  }

}
